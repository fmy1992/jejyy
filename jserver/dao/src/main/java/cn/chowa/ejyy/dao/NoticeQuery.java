package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

/**
 * @ClassName NoticeQuery
 * @Author ironman
 * @Date 17:46 2022/8/17
 */

@JqlQuery
public interface NoticeQuery {

    List<ObjData> getCommunityNotices(long community_id, int published, int page_size, int page_num);

    long getCommunityNoticesCount(long community_id, int published);

    ObjData getCommunityNotice(long id,long community_id);

}
