package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface UserCarQuery {

    List<ObjData> getUserCars(long community_id, Integer is_new_energy, String car_number, Integer car_type, Integer status, Integer sync, int page_size, int page_num);

    long getUserCarsCount(long community_id, Integer is_new_energy, String car_number, Integer car_type, Integer status, Integer sync);

    ObjData getUserCarInfo(long id, long community_id);

    List<ObjData> getUserCarOperateLogs(long id);
}
