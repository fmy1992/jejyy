package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;
import cn.hutool.json.JSONArray;

import java.util.List;

@JqlQuery
public interface BuildingQuery {

    Integer getBindingStatus(long id, long buildingId, long communityId);

    List<ObjData> getBuildingOwner(long buildingId);

    List<ObjData> getBuildingHistoryOperations(long userBuildingId);

    long getBindingTotal(long communityId, int type, int buildingStatus);

    long getOwnerTotal(long communityId, int buildingStatus);

    long getCarTotal(int status, long communityId, int type);

    List<ObjData> getOwnerBuildings(long ownerId, int status, long community_id);

    ObjData getUserBuildingInfo(long wechat_mp_user_id, long building_id, long community_id);

    List<ObjData> getWechatCommunityUserBuilding(int status, long wechat_mp_user_id);

    List<ObjData> getBuildingInfo(long community_id);

    List<ObjData> getBuildingList(long community_id, long wechat_mp_user_id, String content);

    List<ObjData> getBuildInfo(JSONArray building_ids);

    List<ObjData> getBuildings(long community_id, long wechat_mp_user_id, List<Long> bids);

    ObjData getBindingSetting(long building_id, long community_id);
}
