package cn.chowa.ejyy.dao;

import cc.iotkit.jql.ObjData;
import cc.iotkit.jql.annotation.JqlQuery;

import java.util.List;

@JqlQuery
public interface FitmentQuery {

    List<ObjData> getFitments(int step, int is_return_cash_deposit, long community_id, int page_size, int page_num);

    long getFitmentsCount(int step, int is_return_cash_deposit, long community_id);

    long getUnfinishedFitmentCount(long community_id, long building_id);

    ObjData getFitmentDetail(long community_id, long id);

}
