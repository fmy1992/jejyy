package cn.chowa.ejyy.dao.question;

import cn.chowa.ejyy.model.entity.question.Question;
import cn.chowa.ejyy.model.entity.question.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface QuestionRepository extends JpaRepository<Question, Long>, JpaSpecificationExecutor<Question> {

}
