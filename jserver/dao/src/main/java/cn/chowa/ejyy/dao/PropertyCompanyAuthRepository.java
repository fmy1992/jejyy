package cn.chowa.ejyy.dao;

import cn.chowa.ejyy.model.entity.PropertyCompanyAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyCompanyAuthRepository extends JpaRepository<PropertyCompanyAuth, Long> {
}
