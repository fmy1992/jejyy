package cn.chowa.ejyy.notice;

import cc.iotkit.jql.ObjData;
import cn.chowa.ejyy.common.CodeException;
import cn.chowa.ejyy.common.Constants;
import cn.chowa.ejyy.common.RequestData;
import cn.chowa.ejyy.common.annotation.VerifyCommunity;
import cn.chowa.ejyy.dao.NoticeQuery;
import cn.chowa.ejyy.dao.PropertyCompanyUserRepository;
import cn.chowa.ejyy.model.entity.PropertyCompanyUser;
import cn.dev33.satoken.annotation.SaCheckRole;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static cn.chowa.ejyy.common.Constants.code.QUERY_ILLEFAL;
import static cn.chowa.ejyy.common.Constants.tpl.OA_NOTICE_COMMUNITY_USER_STOP_ELECTRICITY;
import static cn.chowa.ejyy.common.Constants.tpl.OA_NOTICE_COMMUNITY_USER_STOP_WATER;

@RestController("noticeDetail")
@RequestMapping("/pc/notice")
public class detail {

    @Autowired
    private NoticeQuery noticeQuery;

    @Autowired
    private PropertyCompanyUserRepository propertyCompanyUserRepository;


    /**
     * 小区通知 - 详情
     */
    @SaCheckRole(Constants.RoleName.XQTZ)
    @VerifyCommunity(true)
    @PostMapping("/detail")
    public Map<String, Object> detail(@RequestBody RequestData data) {
        long id = data.getId();
        long communityId = data.getCommunityId();
        //查询详情
        ObjData detail = noticeQuery.getCommunityNotice(id, communityId);

        if (detail == null) {
            throw new CodeException(QUERY_ILLEFAL, "不存在的通知");
        }
        detail.put("tpl_content", JSONUtil.parse(detail.get("tpl_content")));

        String tpl_title = "非法模板";
        String tpl=detail.getStr("tpl");
        tpl=tpl==null?"":tpl;
        switch (tpl) {
            case OA_NOTICE_COMMUNITY_USER_STOP_WATER:
                tpl_title = "停水通知";
                break;
            case OA_NOTICE_COMMUNITY_USER_STOP_ELECTRICITY:
                tpl_title = "停电通知";
                break;
        }
        String published_real_name = "";

        if (detail.getStr("published_by") != null) {
            //查询，发布人信息
            PropertyCompanyUser pInfo = propertyCompanyUserRepository.findById(detail.getLong("published_by")).get();
            published_real_name = pInfo.getRealName();
        }

        detail.put("tpl_title", tpl_title);
        detail.put("published_real_name", published_real_name);
        return detail;
    }

}
