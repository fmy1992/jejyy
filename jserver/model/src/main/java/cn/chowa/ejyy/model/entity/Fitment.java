package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * 装修登记
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_fitment")
@DynamicUpdate
public class Fitment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("building_id")
    private long buildingId;

    private int step;

    private Long agree_user_id;

    private Long agreed_at;

    private Integer cash_deposit;

    private Long finished_at;

    private Long confirm_user_id;

    private Long confirmed_at;

    private String return_name;

    private String return_bank;

    private String return_bank_id;

    private Long return_operate_user_id;

    private boolean is_return_cash_deposit;

    private Long returned_at;

    private Long created_at;

}
