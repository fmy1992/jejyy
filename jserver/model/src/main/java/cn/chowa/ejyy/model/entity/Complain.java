package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_complain")
public class Complain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("community_id")
    private long communityId;

    /**
     * 1 投诉 2建议
     */
    private int type;

    @JsonProperty("wechat_mp_user_id")
    private Long wechatMpUserId;

    @JsonProperty("property_company_user_id")
    private Long propertyCompanyUserId;

    /**
     * 1 卫生问题；2 噪音问题；3 服务态度；4 违建； 5 占用消防通道； 6 社区设施； 7 其他
     */
    private Integer category;

    private String description;

    private String complain_imgs;

    @JsonProperty("dispose_subscribed")
    private int disposeSubscribed;

    @JsonProperty("confrim_subscribed")
    private int confrimSubscribed;

    @JsonProperty("finish_subscribed")
    private int finishSubscribed;

    @JsonProperty("allot_user_id")
    private Long allotUserId;

    @JsonProperty("alloted_at")
    private Long allotedat;

    @JsonProperty("dispose_user_id")
    private Long disposeUserId;

    @JsonProperty("dispose_reply")
    private String disposeReply;

    @JsonProperty("dispose_content")
    private String disposeContent;

    @JsonProperty("dispose_imgs")
    private String disposeImgs;

    @JsonProperty("disposed_at")
    private Long disposedAt;

    @JsonProperty("finished_at")
    private Long finishedAt;

    @JsonProperty("merge_id")
    private Long mergeId;

    /**
     * 1业主提交; 2 客服调拨; 3维修上门; 4 维修完成; 5评价
     */
    private int step;

    /**
     * 1 - 5
     */
    private Integer rate;

    @JsonProperty("rate_content")
    private String rateContent;

    @JsonProperty("rated_at")
    private Long ratedAt;

    @JsonProperty("created_at")
    private long createdAt;
}
