package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ejyy_property_company_user_login")
public class PropertyCompanyUserLogin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("property_company_user_id")
    private long propertyCompanyUserId;

    private String ip;

    @JsonProperty("user_agent")
    private String userAgent;

    @JsonProperty("login_at")
    private long loginAt;

}
