package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "ejyy_wechat_official_accounts_user")
public class WechatOfficialAccountsUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "unionId不能为空")
    @JsonProperty("union_id")
    private String unionId;

    @NotNull(message = "openId不能为空")
    @JsonProperty("open_id")
    private String openId;

    private int subscribed;

    @JsonProperty("created_at")
    private long createdAt;
}
