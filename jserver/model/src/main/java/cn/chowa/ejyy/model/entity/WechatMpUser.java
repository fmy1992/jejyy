package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "ejyy_wechat_mp_user")
public class WechatMpUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "openId不能为空")
    @Size(max = 28, message = "openId不合法")
    @JsonProperty("open_id")
    private String openId;

    @NotNull(message = "unionId不能为空")
    @Size(max = 28, message = "unionId不合法")
    @JsonProperty("union_id")
    private String unionId;

    @JsonProperty("nick_name")
    private String nickName;

    @JsonProperty("real_name")
    private String realName;

    private String idcard;

    private String phone;

    private String avatarUrl;

    /**
     * 1 男 2女
     */
    @Range(min = 1, max = 2, message = "性别不合法")
    private int gender;

    @NotNull(message = "签名不能为空")
    @Size(max = 56, message = "签名长度不正确")
    private String signature;

    /**
     * 0 身份信息未补全； 1补全
     */
    @Range(min = 0, max = 1, message = "数据不合法")
    private int intact;

    @JsonProperty("created_at")
    private long createdAt;
}
