package cn.chowa.ejyy.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * 用户房产
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "ejyy_user_building")
public class UserBuilding {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JsonProperty("building_id")
    private long buildingId;

    @JsonProperty("wechat_mp_user_id")
    private long wechatMpUserId;

    private Integer authenticated;

    /**
     * 1 实名信息自行关联；2 物业公司认证 3 业主认证
     */
    @JsonProperty("authenticated_type")
    private Integer authenticatedType;

    /**
     * 根据type查询认证用户
     */
    @JsonProperty("authenticated_user_id")
    private Long authenticatedUserId;

    /**
     * 1 正常；0 解绑
     */
    private int status;

    @JsonProperty("created_at")
    private long createdAt;
}
